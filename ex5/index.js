const app = document.querySelector('#app');
const button = document.querySelector('button');

/**
 * Récupère une personne et l'affiche dans l'HTML
 * @param {number} ID Id de la personne
 */
async function fetchPerson(ID) {
    // Person request
    const response = await fetch(`https://swapi.dev/api/people/${ID}`);
    const person = await response.json();
    // Affichage pour que vous puissiez voir à quoi ressemble l'objet
    console.log(person);
    // Le mieux est d'aller voir la documentation : https://swapi.dev/documentation#people

    addPersonToDOM(person);
}

/**
 * Affiche une personne dans l'HTML
 * @param {Object} person La personne à afficher
 */
function addPersonToDOM(person) {
    // Création d'une balise dans le document.
    const personDiv = document.createElement('div');
    // Ajour de la classe `person` à la div
    personDiv.classList.add('person');
    personDiv.textContent = person.name;

    // Ajout de la balise dans la div#app déjà présente dans l'HTML
    app.appendChild(personDiv);
}

let i = 1;
button.addEventListener('click', async () => {
    console.log('Lancement de la requête');
    // Utilisation de await pour attendre que la requête soit bien fini
    await fetchPerson(i);
    i++;
    console.log('Requête terminé');
});
