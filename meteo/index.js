const API_KEY = "9cf3f756f309867de06a7d7f067e9878";

async function fetchWeatherDataFor(city) {
    const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`);
    const data = await response.json();
    console.log(data);
}

fetchWeatherDataFor('Lyon');